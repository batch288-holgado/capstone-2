const express = require('express')
const router = express.Router()
const productsControllers = require('../controllers/productsControllers.js')
const auth = require('../auth.js')

router.post('/createProduct', auth.verify, productsControllers.createProduct)
router.get('/retrieveAllProducts', auth.verify, productsControllers.retrieveAllProducts)
router.get('/retrieveAvailableProducts', productsControllers.retrieveAvailableProducts)
router.get('/retrieveUnavailableProducts', productsControllers.retrieveUnavailableProducts)
router.get('/:productId', productsControllers.retrieveSingleProduct)
router.patch('/:productId', auth.verify, productsControllers.updateProductDescription)
router.patch('/:productId/archiveProduct', auth.verify, productsControllers.archiveProduct)
router.patch('/:productId/unarchiveProduct', auth.verify, productsControllers.unarchiveProduct)
router.patch('/:productId/updateStock', auth.verify, productsControllers.updateStock)
router.patch('/:productId/updatePrice', auth.verify, productsControllers.updatePrice)

module.exports = router