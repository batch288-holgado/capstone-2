const express = require ('express')
const mongoose = require('mongoose')
const cors = require('cors')
const port = 4002
const app = express()
const usersRoutes = require('./routes/usersRoutes.js')
const productsRoutes = require('./routes/productsRoutes.js')

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.listen(port, () => console.log(`Server is now running at port ${port}!`))
app.use(cors())
app.use('/users', usersRoutes)
app.use('/products', productsRoutes)

// Connection to MongoDB
mongoose.connect("mongodb+srv://admin:admin@batch288holgado.3ynebd7.mongodb.net/capstone2?retryWrites=true&w=majority", {
	usenewUrlParser: true,
	useUnifiedTopology: true,
})

const db = mongoose.connection
	db.on("error", console.error.bind(console, "Error, can't connect to the db!"))

	db.once("open", () => console.log("We are now connected to the db"))