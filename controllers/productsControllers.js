const Products = require ('../models/Products.js')
const auth = require ('../auth.js')

module.exports.createProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	console.log(userData)

	if(userData.isAdmin){

		let newProduct = new Products({
			name: request.body.name,
			description: request.body.description,
			isAvailable: request.body.isAvailable,
			stock: request.body.stock,
			price: request.body.price
		})

		newProduct.save()
		.then(result => response.send('Product successfully created'))
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}

}

module.exports.retrieveAllProducts = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization)
	if (userData.isAdmin){
		Products.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))	
	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
	
}

module.exports.retrieveAvailableProducts = (request, response) => {
	Products.find({isAvailable: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

module.exports.retrieveUnavailableProducts = (request, response) => {
	Products.find({isAvailable: false})
	.then(result => response.send(result))
	.catch(error => response.send(error))	
}

module.exports.retrieveSingleProduct = (request, response) => {
	const productId = request.params.productId 
	Products.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

module.exports.updateProductDescription = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updateDescription = {
		description: request.body.description
	}

	if(userData.isAdmin){
		Products.findByIdAndUpdate(productId, updateDescription)
		.then(result => response.send('Description successfully updated!'))
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}

module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let archiveProduct = {
		isAvailable: false
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, archiveProduct)
		.then(result => {
			if (result.isAvailable){
				return response.send(`${result.name} is now archived.`)
			}else{
				return response.send(`${result.name} is already archived.`)
			}
		})
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}

module.exports.unarchiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let archiveProduct = {
		isAvailable: true
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, archiveProduct)
		.then(result => {
			if (!result.isAvailable){
				return response.send(`${result.name} has been retrieved from the archive.`)
			}else{
				return response.send(`${result.name} is not archived.`)
			}
		})
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}

module.exports.updateStock = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updateStock = {
		stock: request.body.stock
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updateStock)
		.then(result => response.send(`${result.name}'s stock has been updated`))
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}

module.exports.updatePrice = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	let updatePrice = {
		price: request.body.price
	}

	if(userData.isAdmin){

		Products.findByIdAndUpdate(productId, updatePrice)
		.then(result => response.send(`${result.name}'s price has been updated`))
		.catch(error => response.send(error))

	}else{
		return response.send('Token provided is not an admin. You do not have access to this route.')
	}
}