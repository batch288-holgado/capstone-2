const Users = require ('../models/Users.js')
const Products = require ('../models/Products.js')
const auth = require ('../auth.js')
const bcrypt = require ('bcrypt')

module.exports.registerUser = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {

		if (result !== null){
			return response.send(`${request.body.email} has been taken. Try logging in or use a different email in signing up!`)
		}else{

			let newUser = new Users ({

				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin,
				mobileNo: request.body.mobileNo

			})

			newUser.save()
			.then(saved => response.send(`${request.body.email} is now registered!`))
			.catch(error => response.send(error))
		}

	}).catch(error => response.send(error))

}

module.exports.getAllUsers = (request, response) => {
	Users.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

module.exports.loginUser = (request, response) => {

	Users.findOne({email: request.body.email})
	.then(result => {

		if(!result){
			
			`${request.body.email} is not yet registered`

		}else{

			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password)

			if(isPasswordCorrect){

				return response.send({
					auth: auth.createAccessToken(result)
				})
			}else{
				return response.send('Please check your password.')
			}
		}
	})
}

module.exports.orderProduct = (request, response) => {

	const productId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send("Admin cannot buy products!");
	}else{

	let isUserUpdated =  Users.findOne({_id : userData.id})
		.then(result => {

			result.orders.push({
				productId : request.body.id
			})

			result.save()
			.then(saved => true)
			.catch(error => {
				console.log(error)
				return false})


		})
		.catch(error => {
				console.log(error)
				return false})

	let isProductUpdated =  Products.findOne({_id : productId})
		.then( result => {

			result.owners.push({
				userId : userData.id
			});

			result.save()
			.then(saved => true)
			.catch(error => {
				console.log(error)
				return false})
		})
		.catch(error => {
				console.log(error)
				return false})

		if(isUserUpdated && isProductUpdated){
			return response.send('Order is successful');
		}else{
			return response.send('Order error, please try again!')
		}
	}
}

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}else{
		return response.send('Token provided is not an admin. You do not have access to this route')
	}
		
}

module.exports.setAsAdmin = (request, response) => {

	const userData = auth.decode(request.headers.authorization)
	const userId = request.params.userId
	let setAdmin = {
		isAdmin: true
	}

	if(userData.isAdmin){
		Users.findByIdAndUpdate(userId, setAdmin)
		.then(result => {
			if(!result.isAdmin){
				return response.send(`${result.firstName} is now an admin.`)
			}else{
				return response.send(`${result.firstName} is already an admin.`)
			}
		})
		.catch(error => response.send(error))
	}else{
		return response.send('Token provided is not an admin. You do not have access to this route')
	}

}
// Note: If you do this, you also have to login the user again and generate a new token (to set as a variable) for the user to be recognized as an admin.

module.exports.retrieveUserOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	
	if(userData.isAdmin){
		Users.findById(request.params.userId)
		.then(result => response.send(result.orders))
		.catch(error => response.send(error))
	}else{
		return response.send('Token provided is not an admin. You do not have access to this route')
	}
		
}

module.exports.getUserOrders = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin){
		Users.find({}).select('firstName lastName orders')
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}else{
		return response.send('Token provided is not an admin. You do not have access to this route')
	}

}